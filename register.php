<?php
session_start();

if(!isset($_SESSION))

?>

<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <title>UScopeX Register</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="styles/styles.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   
</head>
<style>
   
    </style>

<body>
    <div class="signup-form">
        <form method="POST" action="register.php" >
            <h2 style="color:red">Register</h2>
            <p class="hint-text">Create your account</p>
            <div class="form-group">
                <div class="row">
                    <div class="col-xs-6"><input type="text" class="form-control" name="first_name" placeholder="First Name" required="required"></div>
                    <div class="col-xs-6"><input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required"></div>
                </div>
            </div>
            <div class="form-group">
                <input type="email" class="form-control" name="email" placeholder="Email" required="required">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="Password" required="required">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="required">
            </div>
   
            <div class="form-group">
                <button type="submit" class="btn btn reg btn-lg btn-block">Get Started</button>
            </div>
            <div class="text-center" style="color: red;">Already have an account? <a style="color: blue;" href="login.php">Sign in</a></div>
        </form>
        
        <?php
        if (isset($_POST['first_name'])) {
            
            include("connections/conn.php");
            
            // POST data vars
            $firstname = $_POST['first_name'];
            $lastname = $_POST['last_name'];
            $useremail = $_POST['email'];
            $userpassword =$_POST['password'];
            $confirmpassword = $_POST['confirm_password'];

            // Email confirmation fields
            $to = $useremail;
            $subject = "Welcome to UScopeX";
            $message = "Your registration was successful";
            $headers = "From: The sender name <sender@uscopex.org>";

            // Prepared statements here

            $insertuser = $conn->prepare('INSERT INTO Login
            (Email,Password)
            VALUES(?,?)');
            $email_validation = $conn->prepare('SELECT * FROM Login WHERE Email = ?');
            $insert_id = $conn->prepare('INSERT INTO Account_Information (login_ID, First_Name,Last_Name) VALUES (?,?,?)');
                
            // Running prepared statement [EMAIL VALIDATION]
            $email_validation->bind_param("s",$useremail);
            $email_validation->execute();
            $email_validation->store_result();
            $num_rows_returned_email = $email_validation->num_rows;
            $email_validation -> close();

            // Validate password strength(CODEXWORLD CODE)

            $uppercase = preg_match('@[A-Z]@', $userpassword);
            $lowercase = preg_match('@[a-z]@', $userpassword);
            $number    = preg_match('@[0-9]@', $userpassword);
            $specialChars = preg_match('@[^\w]@',$userpassword);

           if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($userpassword) < 8) {
             echo "<p style='color: white;'> Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.</p>";
           }else{
               echo 'Strong password.';
            
            // Check passwords match

            if ($userpassword != $confirmpassword) {
                echo "<p style='color: white;'>Your passwords do not match</p>";
            } else {
                if ($num_rows_returned_email>0) {
                    echo "<p style='color: white;'>Username already in use</p>";
                } else {

                    //begin transaction

                    $conn->begin_transaction();
                    
                    //Runing prepared statment [Inserting User]
                  
                    $insertuser -> bind_param("ss", $useremail, $userpassword);
                    $insertuser -> execute();
                    $insertuser -> close();
    
                    $login_id = $conn->insert_id;
                               
                    //Runing prepared statment [Inserting Login ID to Account_Information Table]

                    $insert_id -> bind_param('iss',$login_id,$firstname,$lastname);
                    $insert_id -> execute();
                    $insert_id -> close();

                    //if both queries have executed succesfully we will commit (else rollback the whole transaction)

                    if ($insertuser && $insert_id) {
                        $conn->commit();
                        mail($to, $useremail, $subject, $message, $headers);
                        $_SESSION['authenticated']=$login_id;
                        header('location:dashboard/dashboard.php');
                    } else {
                        $conn->rollback();

                        echo "There was a problem, please wait and try again";
                    }
                }
            }
        }
    }
        ?>
    </div>
</body>

</html>