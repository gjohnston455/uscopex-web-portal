<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

  <title>UScopeX Landing Page</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/cover/">

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">


  <!-- Custom styles for this template -->
  <link href="styles/index.css" rel="stylesheet">
</head>

<body class="text-center">

  <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">

      <div class="inner">
        <h3 class="masthead-brand">μScopeX</h3>
        <nav class="nav nav-masthead justify-content-center">
          <a class="nav-link" href="register.php">Register</a>
        </nav>
      </div>
    </header>

    <main role="main" class="inner cover">
      
      <div class="container" style="padding-bottom: 5%;">
      <h1 class="cover-heading" style="color: rgba(46, 50, 56, 0.733);">μScopeX Online Portal</h1>
      </div>
    </main>
      <div class="row" style="margin-top: 30%;"></div>
      <div>
        <a href="dashboard/dashboard.php"  class="btn btn-lg btn-secondary">Dashboard</a>
      </div>

    <footer class="mastfoot mt-auto">
      <div class="inner">

      </div>
    </footer>
  </div>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script>
    window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
  </script>
  <script src="../../assets/js/vendor/popper.min.js"></script>
  <script src="../../dist/js/bootstrap.min.js"></script>
</body>

</html>