<?php
session_start();

if(!isset($_SESSION))


?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>UScopeX Login In</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<link rel="stylesheet" href="styles/loginstyles.css">

</head>
<body>
<div class="login-form">
    <form method='POST' action='login.php'>
        <h2 class="text-center" style="color:whitesmoke;"><strong>Sign In</strong></h2>       
        <div class="form-group">
            <input type="email" class="form-control" placeholder="Username" required="required" name ='userfield'>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" placeholder="Password" required="required" name='passfield'>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-secondary btn-block">Log in</button>
        </div>
        <div class="clearfix">
            <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
            <a href="#" class="pull-right">Forgot Password?</a>
        </div>        
    </form>

    
    <?php
                    if (isset($_POST['userfield'])) {
                        include("connections/conn.php");

                        $username = $_POST['userfield'];
                        $passw = $_POST['passfield'];
                      
                        $authentication = $conn->prepare('SELECT ID FROM Login WHERE Email = ? AND Password = ?');


                        $authentication-> bind_param("ss",$username,$passw);
                        $authentication->execute();
                       
                         
                        $authentication -> store_result();
                        $numrows =$authentication->num_rows;  
                        $authentication -> bind_result($login_ids); 
                        
                        $authentication->fetch();
                        $authentication->close();
    
                        if ($numrows>0) {

                            $_SESSION['authenticated'] = $login_ids;
                            header('location:dashboard/dashboard.php');
                        } else{
                            echo 
                            "<div><p>
                            Incorrect details. Try again
                            </p></div>";
                        }
                  }
                ?>
    <p class="text-center"><a href="register.php">Create an Account</a></p>
</div>
</body>
</html>            