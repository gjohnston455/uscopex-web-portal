<?php
session_start();

if (!isset($_SESSION['authenticated'])) {
    header('location:../login.php');
}


if (isset($_SESSION['authenticated'])) {
    $login_id = $_SESSION['authenticated'];
}

include("../connections/conn.php");


?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Results</title>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- Custom styles for this template -->
    <link href="uploadstyles/styles.css" rel="stylesheet">
</head>

<script src="js/dashboard.js" type="text/javascript"></script>
<script>

$(document).ready(function () {
    $('#show').click(function () {
        $('.menu').toggle("slide");
    });
});
$(document).ready(function () {
    $('#show2').click(function () {
        $('.menu2').toggle("slide");
    });
});



function change() {
    var btn = document.getElementById("show");
    if (btn.value == "Show IAM Results") {
        btn.value = "Hide IAM Results";
        btn.innerHTML = "Hide IAM Results";
    }
    else  {
        btn.value = "Show IAM Results";
        btn.innerHTML = "Show IAM Results";
      }
}


function change2() {
    var btn = document.getElementById("show2");

    if (btn.value == "Show ICM Results") {
        btn.value = "Hide ICM Results";
        btn.innerHTML = "Hide ICM Results";
    } else {
        btn.value = "Show ICM Results";
        btn.innerHTML = "Show ICM Results";
    }
}

</script>
<body>

    <header>
        <div class="collapse bg-dark" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-7 py-4">
                        <h4 class="text-white">Get started</h4>
                        <p class="text-muted">
                            Upload your image analysis pipeline. Please ensure to follow the instructions on how to properly script and save the file. Use the provided framework to house your pipeline.
                        </p>
                    </div>
                    <div class="col-sm-4 offset-md-1 py-4">

                        <ul class="list-unstyled">
                            <li><a href="../logout.php" class="text-white">Log out</a></li>
                            <li><a href="#" class="text-white">Report a problem</a></li>
                            <li><a href="dashboard.php" class="text-white">Back to base</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar box-shadow"style="  background-color: rgba(14, 97, 145, 0.877); " >
            <div class="container d-flex justify-content-between"style="">
                <a href="#" class="navbar-brand d-flex align-items-center">
                  
                    <strong>Analysis results</strong>
                </a>
                <button class="navbar-toggler" style="background-color: rgb(10, 75, 75);" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    
                </button>
            </div>
        </div>
    </header>

    <main role="main">


        <section class="jumbotron text-center" style="background-color: rgb(60, 49, 49);">
            <div class="container">
                <h1 class="jumbotron-heading" style="color:  rgb(61, 118, 141);">View Results</h1>
                <p class="lead text-muted">Click to show your result sets.</p>
                <button type="button" class="btn "id="show"onclick="change()" value="Show IAM Results">Show IAM Results</button>
                <button type="button" class="btn "id="show2"onclick="change2()" value="Show ICM Results">Show ICM Results</button>
             
           
            </div>

            <!-- Upload pipeline modal -->
            <div class="modal fade" id="pipeline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body modal-body2">


                            <div class='row'>
                                <div class='col'>
                                    <form action='pipelineuploader.php' method="POST" enctype='multipart/form-data'>

                                        <div class="form-group">

                                            <input type="file" class="form-control-file" name="myfileupload" id="exampleFormControlFile1">
                                        </div>

                                </div>
                            </div>
                            <div class='row' style="padding-bottom: 5%;">
                                <div class='col' id="pop">
                                    <input type="text" class="form-control" id="name" name="pipeline_name" placeholder="Name" aria-label="Search">
                                </div>

                            </div>
                            <div class='row' style="padding-bottom: 5%;">
                                <div class='col'>
                                    <input type="text" class="form-control" id="pipeline_meta" name="version" placeholder="Version" aria-label="Search">
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col'>
                                    <div class="form-group">

                                        <textarea class="form-control" id="description" name="description" rows="3" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col-sm' style="margin-right: 30%;">
                                    <div class="form-group form-check">
                                        <input type="checkbox" name="public" class="form-check-input" id="exampleCheck1" value="Yes">
                                        <label class="form-check-label" for="exampleCheck1">Public</label>
                                    </div>
                                </div>
                                <div class='col-sm'></div>
                                <div class='col-sm'></div>
                            </div>

                            <div class='row' style="padding-bottom: 5%;">
                                <div class='col'>
                                    <input type="email" class="form-control" id="user_access" name="user_access" placeholder="Grant user access (email)" aria-label="Search">
                                </div>
                            </div>

                            <div class='row' style="padding-bottom: 5%;">
                                <div class='col'>
                                    <input type="text" class="form-control" id="company_access" name="new_passw" placeholder="Grant company access (company name)" aria-label="Search">
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col'>
                                    <button type="submit" name="fileup" style="float: left; background-color:  rgba(63, 40, 32, 0.863); color: whitesmoke;" id="upload" class="btn ">Upload</button>

                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="modal-footer">

                            <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>
            </div>

          
        </section>


        <section class=" text-center">
            <div class="container" >
                
            </div>
        </section>
        
        <div class="album py-5 bg-light menu" style="display: none;" >
            <div class="container">

                <div class="row">

                    <?php 

                        $get_iam_results = "SELECT * FROM iam_result_set WHERE User_Id = '$login_id'";
                        $result= $conn->query($get_iam_results);
                        while($row=$result->fetch_assoc()){
                            $iam_name = $row['Name'];
                            $iam_result_path = $row['Result_Path'];       
                            $iam_img_path = $row['Image_Path'];
                            $iam_date= $row['Date'];
                            $pipeline = $row['Pipeline'];
                            $pipeline_exp = explode('.',$pipeline);
                            $pipeline = $pipeline_exp[0];

                        

                        echo " <div class='col-md-4'>
                        <div class='card mb-4 box-shadow' style='background-color:  rgba(200, 236, 216, 0.788);'>

                            <div class='card-body'>
                                <p class='card-text'><span style='color:green;'>Analysis Name</span>: $iam_name</p>
                                <p class='card-text'><span style='color:green;'>Pipeline</span>: $pipeline</p>
                                <p class='card-text' style=' width: max-content;'><span style='color:green;'>Description</span></p>
                                <p class='card-text'><span style='color:green;'>Date</span>: $iam_date</p>
                                <div class='d-flex justify-content-between align-items-center' style='margin-top: 0px;'>
                                <div class='btn-group' style='margin-top: 0px;'>
                                <p>Results &ensp;</p><a href=../IAM_Result_Sets/$iam_result_path download><button type='button' data-id=''><i class='fas fa-file-download'></i></button></a><br>
                                <p>&ensp; Image &ensp; </p><a href=../processed_images/$iam_img_path download><button type='button' data-id=''><i class='fas fa-file-download'></i></button></a>
                            </div>
                        </div>                            
                    </div>
                </div>
            </div>
                       "
                    } ?>



                </div>
            </div>
        </div>
        <div class="album py-5 bg-light menu2" style="display: none;" >
            <div class="container">

                <div class="row">

                    <?php

                        $get_icm_results = "SELECT i.Name,i.Description, i.Date, i.Classification , m.Name AS Model,  m.ClassNames
                        FROM ICM_Result i
                        INNER JOIN Model m on i.Model = m.ID
                        WHERE User_ID = '$login_id'";
                        $result= $conn->query($get_icm_results);
                        while($row=$result->fetch_assoc()){
                            $icm_name = $row['Name'];
                            $icm_description = $row['Description'];       
                            $icm_classification = $row['Classification'];
                            $icm_date= $row['Date'];
                            $model = $row['Model'];
                            $classNames = $row['ClassNames'];
                            $classNamesArr = str_getcsv($classNames,',');

                            $classification = $classNamesArr[$icm_classification];
                        echo " <div class='col-md-4'>
                        <div class='card mb-4 box-shadow' style='background-color:  rgba(200, 236, 216, 0.788);'>

                            <div class='card-body'>
                                <p class='card-text'><span style='color:green;'>Classification Name</span>: $icm_name</p>
                                <p class='card-text'><span style='color:green;'>Model Used</span>: $model</p>
                                <p class='card-text'><span style='color:green;'>Result</span>: $classification</p>                             
                                <p class='card-text' style=' width: max-content;'><span style='color:green;'>Description</span> </p>                                
                                <p class='card-text'><span style='color:green;'>Date</span>: $icm_date</p>
                                <div class='d-flex justify-content-between align-items-center' style='margin-top: 0px;'>
                                <div class='btn-group' style='margin-top: 0px;'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                       ";
                    } ?>
                </div>
            </div>
        </div>

    </main>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous">
    </script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
</body>

</html>