<?php

use phpDocumentor\Reflection\DocBlock\Description;

include('../connections/conn.php');


if (isset($_POST['id'])) {
    $response = "";
    $scale = $_POST['scale'];
    $roi = $_POST['roi'];
    $id = $_POST['id'];
    $description = $_POST['version_description'];
    $version_num = $_POST['version_number'];
    $image = $_POST['image'];
    $results = $_POST['results'];


    if (0 < $_FILES['file']['error']) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    } else {
        $conn->begin_transaction();

        $get_current_info = "SELECT Name, Versions FROM Pipeline WHERE ID= '$id'";
        $result = $conn->query($get_current_info);

        while ($row = $result->fetch_assoc()) {
            $name = $row['Name'];
            $versions = $row['Versions'];
        }
        $new_path_name = $name . "_" . $version_num . ".bsh";

        $response .= $new_path_name;

        $moved =  move_uploaded_file($_FILES['file']['tmp_name'], '../../iam_pipeline/' . $new_path_name);
        if ($moved) {
            $response .= "Updated. New path name : " . $new_path_name;
            $Version_Up = $conn->prepare('INSERT INTO Pipeline_Version (Pipeline,Version,Description,Date) VALUES (?,?,?,NOW())');
            $Version_Up->bind_param("iss", $id, $version_num, $description);

            if ($Version_Up->execute()) {
                $version_id = $conn->insert_id;
                $insert_requirements = $conn->prepare('INSERT INTO Pipeline_Requirements (Pipeline_ID, Requires_Roi, Requires_Scale) VALUES (?,?,?)');
                $insert_requirements->bind_param("iii", $version_id, $roi, $scale);
                if ($insert_requirements->execute()) {
                    $response .= "Updated requirements";
                    $new_count = $versions + 1;
                    $response = "Inserted new version in version table... ";
                    $update_version_count = $conn->prepare('UPDATE Pipeline SET Versions = ? WHERE ID = ?');
                    $update_version_count->bind_param("ii", $new_count, $id);
                    if ($update_version_count->execute()) {
                        $response .= "Version count updated";
                        $insert_outputs = $conn->prepare('INSERT INTO Pipeline_Output (Pipeline, Image, Result_Set) VALUES (?,?,?)');
                        $insert_outputs->bind_param("iii", $version_id, $image, $results);
                        if ($insert_outputs->execute()) {
                            $response .= "Ouptus added";
                            $conn->commit();
                        } else {
                            $response .= "Outputs failed";
                            $conn->rollback();
                        }
                    } else {
                        $response .= "version count failed to update.";
                        $conn->rollback();
                    }
                } else {
                    echo "failed to update requirements";
                    $conn->rollback();
                }
            } else {
                $response = "Failed to insert new version in version table... ";
            }
        } else {
            $response .= "File failed to move";
        }
    }
}

echo $response;
