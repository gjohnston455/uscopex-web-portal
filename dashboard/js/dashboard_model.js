
    $(document).ready(function validateUser() {
        console.log("Ready");
        $("#name").change(function() {
            var usr = $("#name").val();

            if (usr.length >= 3) {
                
                console.log(usr);

                $.ajax({
                    type: "POST",
                    url: "check_model.php",
                    data: {
                        'name': usr,
                    },
                    success: function(msg) {
                        console.log(msg);
                     
                        if (msg == 'OK') {   
                                $('#name').popover({
                                    content: "Unique name"
                                });
                                $('#name').popover('show');
                               
                            } else {
                                $('#name').popover({
                                    content: "Name already used"
                                }).popover("show");
                                $('.popover').addClass('popover-danger');
                                $("#name").removeClass('object_ok'); // if necessary
                                $("#name").addClass("object_error");
                                $(this).html(msg);
                            }
                            setTimeout(function(){
                                    console.log("ping");
                                    $(".popover:visible").popover('dispose');
                                },2000);
                    }
                });
            } 
        
    });
});

$(document).ready(function validateChangeName() {
    console.log("Ready");
    $("#change_name").change(function() {
        var usr = $("#change_name").val();

        if (usr.length >= 3) {
            
            console.log(usr);

            $.ajax({
                type: "POST",
                url: "check_model.php",
                data: {
                    'name': usr,
                },
                success: function(msg) {
                    console.log(msg);
                 
                    if (msg == 'OK') {   
                            $('#change_name').popover({
                                content: "Unique name"
                            });
                            $('#change_name').popover('show');
                           
                        } else {
                            $('#change_name').popover({
                                content: "Name already used"
                            }).popover("show");
                            $('.popover').addClass('popover-danger');
                            $("#change_name").removeClass('object_ok'); // if necessary
                            $("#change_name").addClass("object_error");
                            $(this).html(msg);
                        }
                        setTimeout(function(){
                                console.log("ping");
                                $(".popover:visible").popover('dispose');
                              
                            },2000);
                }
            });
        } 
    
});
});

$(function changeDetails() {

    console.log("ready to change details!");

    $(document).on('click', '.editer', function(e) {

        e.preventDefault(); 
        var pipeline_id = $(this).data('id');
        console.log(pipeline_id);   
        $(document).on('click', '.change', function(e) {
            var change_name = $('#change_name').val();
            var change_version = $('#change_version').val();
            var change_description = $('#change_desc').val();
        $.ajax({
            url: 'change_model.php',
            type: 'post',
            data: {
                'pipeline_id': pipeline_id,
                'change_name': change_name,
                'change_version': change_version,
                'change_description': change_description,
            },
            success: function(response) {
                console.log(response);
                $(".modal-body4 input").val("");

                if (response == 'Name_Up') {   
                    $(".modal-body4 input").val("");
                    $('#change_name').popover({
                        content: "Changed"
                    });
                    $('#change_name').popover('show');
                   
                } else if(response == 'ALL'){
                    $(".modal-body4 input").val("");
                    $('#change_name').popover({
                        content: "Changed"
                    });
                    $(".modal-body4 input").val("");
                    $('#change_version').popover({
                        content: "Changed"
                    });
                    $('#change_version').popover('show');
                    document.getElementById("change_desc").value = "";
                    $('#change_desc').popover({
                        content: "Changed"
                    });
                    $('#change_desc').popover('show');
                }else if(response == 'Name_Version'){
                    $(".modal-body4 input").val("");
                    $('#change_name').popover({
                        content: "Changed"
                    });
                    $(".modal-body4 input").val("");
                    $('#change_version').popover({
                        content: "Changed"
                    });
                }else if(response == 'Name_Description'){
                    $(".modal-body4 input").val("");
                    $('#change_name').popover({
                        content: "Changed"
                    });
                    document.getElementById("change_desc").value = "";
                    $('#change_desc').popover({
                        content: "Changed"
                    });
                    $('#change_desc').popover('show');
                }else if(response == 'Version_Description'){
                    $(".modal-body4 input").val("");
                    $('#change_version').popover({
                        content: "Changed"
                    });
                    document.getElementById("change_desc").value = "";
                    $('#change_desc').popover({
                        content: "Changed"
                    });
                    $('#change_desc').popover('show');
                    
                }else if(response == 'Name_Fail') {
                    $(".modal-body4 input").val("");
                    $('#change_name').popover({
                        content: "Failed to change name"
                    }).popover("show");
                    $('.popover').addClass('popover-danger');
                   
                    
                } else if(response == 'Version_Up'){
                    console.log("in here");
                    $(".modal-body4 input").val("");
                    $('#change_version').popover({
                        content: "Changed"
                    });
                    $('#change_version').popover('show');

                }else if(response == 'Version_Fail'){
                    $(".modal-body4 input").val("");
                    $('#change_version').popover({
                        content: "Changed"
                    });
                    $('#change_version').popover('show');

                }else if(response == 'Description_Up'){
                    document.getElementById("change_desc").value = "";
                    $('#change_desc').popover({
                        content: "Changed"
                    });
                    $('#change_desc').popover('show');

                }else if(response == 'Description_Fail'){
                    document.getElementById("change_desc").value = "";
                    $('#change_desc').popover({
                        content: "Changed"
                    });
                    $('#change_desc').popover('show');

                }
                setTimeout(function(){
                        console.log("ping");
                        $(".popover:visible").popover('dispose');
                        
                    },2000);
            }
            //close AJAX
        });
        //close event handler
    });
    });
});

$(function versionUpdate() {

    console.log("ready to update version!");
    
    
    
    
    $(document).on('click', '.version_updater_btn', function(e) {
    
        e.preventDefault(); // here using code stop post
        console.log("version updater clicked");
    
        var id = $(this).data('id');
        console.log(id);
       
        $(document).on('click', '#updater_btn', function(e) {
            var version_number = $('#version_number_model').val();
            var version_description = $('#version_description').val();
            var file_data = $('#versioner').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('id', id);
            form_data.append('version_number',version_number);
            form_data.append('version_description',version_description);
            console.log(form_data);
        // AJAX request
        $.ajax({
            url: 'updateversion_model.php',
            type: 'post',
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(response) {
                console.log(response);

            }
            //close AJAX
        });
        //close event handler
    
    });
    });
    });  

    $(function checkAccess() {

        console.log("ready to check access!");
        
        
        
        
        $(document).on('click', '.access_manager', function(e) {
        
            e.preventDefault(); // here using code stop post
            console.log("access updater clicked");
        
            var check_id = $(this).data('id');
            console.log(check_id);
        
            // AJAX request
            $.ajax({
                url: 'check_model.php',
                type: 'post',
              
                data:{ 'check_id': check_id,
            },
                success: function(response) {
                   
                    console.log(response);

                      if (response==0) {
                          console.log("got 0");
                        // turn toggle switch off
                        $("#option1").attr("checked", false);
                        $('#option2').attr("checked", true);
                     //   $('#activate').click();
                      } else {
                        // turn toggle switch ON
                        $("#option1").attr("checked", true);
                        $('#option2').attr("checked", false);
                     //   $('#activate').click();
                      }
                    
                }
                //close AJAX
            });
            //close event handler
        
        });
        });

        $(function changeAccessLevel() {

            console.log("ready to change access level!");
        
        
        
        
            $(document).on('click', '.access_manager', function(e) {
        
                e.preventDefault(); // here using code stop post
        
                var pipeline_id_access = $(this).data('id');
                console.log(pipeline_id_access);
               
                $(document).on('click', '.access_manager_change', function(e) {
                    console.log("change access clicked");

                    var access_setting = $(this).data('id');
                    console.log(access_setting);
        
        
                // AJAX request
                $.ajax({
                    url: 'change_model.php',
                    type: 'post',
                    data: {

                        'pipeline_id_access': pipeline_id_access,
                        'access_setting': access_setting,
        
                    },
                    success: function(response) {
                        console.log(response);

                        if (response == 'OK') {   
                            $('#access_change').popover({
                                content: "Changed"
                            });
                            $('#access_change').popover('show');
                           
                        } else {
                            $('#access_change').popover({
                                content: "Failed to change"
                            }).popover("show");
                            $('.popover').addClass('popover-danger');
                            $("#access_change").removeClass('object_ok'); // if necessary
                            $("#access_change").addClass("object_error");
                            $(this).html(msg);
                        }
                        setTimeout(function(){
                                console.log("ping");
                                $(".popover:visible").popover('dispose');
                            },2000);
                    }
                    //close AJAX
                });
                //close event handler
        
            });
            });
        });
    

        $(function addAccess() {

            console.log("ready to change details!");
        
            $(document).on('click', '.access_manager', function(e) {
        
                e.preventDefault(); // here using code stop post
        
                var pipeline_id_access = $(this).data('id');
                console.log('pipeline_id_access');
                console.log(pipeline_id_access);
               
                $(document).on('click', '.user_access_btn', function(e) {
        
                    var add_user_access = $('#add_user_access').val();
                    var add_company_access = $('#add_company_access').val();
                    console.log(add_user_access);
                   
        
                // AJAX request
                $.ajax({
                    url: 'change_model.php',
                    type: 'post',
                    data: {
        
                        'pipeline_id_access': pipeline_id_access,
                        'add_user_access': add_user_access,
                        'add_company_access': add_company_access,
                       
                    },
                    success: function(response) {
                        console.log(response);
                        if (response == 'OK') {   
                            $('#add_user_access').popover({
                                content: "Success"
                            });
                            $('#add_user_access').popover('show');
                           
                        } else if(response == '101'){
                            $('#add_user_access').popover({
                                content: "User already has access"
                            });
                            $('#add_user_access').popover('show');

                        }else {
                            $('#add_user_access').popover({
                                content: "User doesn't exist"
                            }).popover("show");
                            $('.popover').addClass('popover-danger');
                            $("#add_user_access").removeClass('object_ok'); // if necessary
                            $("#add_user_access").addClass("object_error");
                            $(this).html(response);
                        }
                        setTimeout(function(){
                                console.log("ping");
                                $(".popover:visible").popover('dispose');
                            },2000);
                    }
                    //close AJAX
                });
                //close event handler
        
            });
            });
        });
       

    