<?php

$response = "";
include('../connections/conn.php');

if(isset($_POST['pipeline_id'])){

    $pipeline_id = $_POST['pipeline_id'];
    $change_name = $_POST['change_name'];
    $version = $_POST['change_version'];
    $new_desc = $_POST['change_description'];
    if (
        isset($_POST['public']) &&
        $_POST['public'] == 'Yes'
    ) {
        $public = 1;
    } else {
        $public = 0;
    }
   
    if(!empty($change_name)){
        $change_name = $_POST['change_name'];
        $get_current_name = $conn->prepare('SELECT Pipeline_Path FROM Pipeline WHERE ID =?');
        $change_name_query = $conn->prepare('UPDATE Pipeline SET Name = ? , Pipeline_Path = ? WHERE ID =?');
        $get_current_name->bind_param("i",$pipeline_id);
       
        if($get_current_name->execute()){
            $get_current_name->store_result();
            $get_current_name->bind_result($path);
            $get_current_name->fetch();
            $get_current_name->close();
            

            $new_path = $change_name . ".bsh";
            $rename = rename("../iam_pipeline/$path","../iam_pipeline/$new_path");
            if($rename){
           
                $change_name_query->bind_param("ssi",$change_name,$new_path,$pipeline_id);

                if($change_name_query->execute()){
                    $response.= "Name_Up";
                }else{
                    $response .=$conn->error;
                }
            }else{
                $response .= "Name_Fail";
            }
        }
        
        
    }
    if(!empty($new_desc)){
        $update_desc = $conn->prepare('UPDATE Pipeline SET Description = ? WHERE ID =?');
        $update_desc->bind_param("si",$new_desc,$pipeline_id);
        if($update_desc->execute()){
            $response.= "Description_Up";
        }else{
            $response.= "Description_Fail";
        }
    }



}

if(isset($_POST['access_setting'])){
    $access_setting = $_POST['access_setting'];
    $id = $_POST['pipeline_id_access'];

    $change_setting ="UPDATE Pipeline SET Is_Public = '$access_setting' WHERE ID ='$id'";
    $result = $conn->query($change_setting);
    if(!$result){
        echo $conn->error;
    }else{
        $response .= "OK";
    }
}


if(isset($_POST['add_user_access'])){
    $user_access_email = $_POST['add_user_access'];
    $pipe_id = $_POST['pipeline_id_access'];
 
   
   $check_current_access = $conn->prepare('SELECT * FROM Pipeline_Access WHERE Pipeline_ID = ? AND User_ID = (SELECT ID From Login WHERE Email = ?)');
   $check_current_access->bind_param("is",$pipe_id,$user_access_email);


    if($check_current_access->execute()){
        $check_current_access->store_result();
        $num_rows = $check_current_access->num_rows();
        $check_current_access->close();
      

        if($num_rows<1){
        
        $add_new_user_access = "INSERT INTO Pipeline_Access (Pipeline_ID, User_ID) VALUES ('$pipe_id',(SELECT ID FROM Login WHERE Email = '$user_access_email'))";
            $result = $conn->query($add_new_user_access);
            if($result){
            $response.= "OK";
        } else {
            $response.= "Failure";
        }
    }else{
        $response .= "101";
    }

    }else{
        $response .= "Problem";

    }
 
}
if($response == 'Name_UpVersion_UpDescription_Up'){
    $response = "ALL";
}else if($response == 'Name_UpVersion_Up'){
    $response = "Name_Version";
}else if($response == 'Name_UpDescription_Up'){
    $response = "Name_Description";
}else if($response == 'Version_UpDescription_Up'){
    $response = "Version_Description";
}


echo $response;


?>
