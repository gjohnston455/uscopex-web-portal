<?php
include('../connections/conn.php');
if (isset($_POST['id'])) {
    
    $response = "";

    $id = $_POST['id'];
    $description = $_POST['version_description'];
    $version_num = $_POST['version_number'];
    $response .= "  vnum  : "+$version_num;
    $response .= "id .. ".$id;
    if (0 < $_FILES['file']['error']) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    } else {
        $conn->begin_transaction();

        $get_current_info = "SELECT Name, Path, Versions FROM Model WHERE ID= '$id'";
        $result = $conn->query($get_current_info);

        while ($row = $result->fetch_assoc()) {
            $name = $row['Name'];
            $path_name = $row['Path'];
            $versions = $row['Versions'];
            
        }
        $response .= "name .." .$name;
        $new_path_name = $name . "_" . $version_num . ".bsh";

        $response .= $new_path_name;

        $moved =  move_uploaded_file($_FILES['file']['tmp_name'], '../icm_model/' .$new_path_name);
        if ($moved) {
            $response .= "Updated. New path name : " .$new_path_name;
            $Version_Up = $conn->prepare('INSERT INTO Model_Version (Model,Version,Description,Date) VALUES (?,?,?,NOW())');
            $Version_Up->bind_param("ids", $id, $version_num, $description);

            if ($Version_Up->execute()) {
                $new_count = $versions + 1;
                $response = "Inserted new version in version table... ";
                $update_version_count = $conn->prepare('UPDATE Model SET Versions = ? WHERE ID = ?');
                $update_version_count->bind_param("ii", $new_count, $id);
                if ($update_version_count->execute()) {
                    $response .= "Version count updated";
                    $conn->commit();
                } else {
                    $response .= "version count failed to update.";
                    $conn->rollback();
                }
            } else {
                $response .= "Failed to insert new version in version table... ";
                echo $conn->error;
            }
        }else{
            $response .= "File failed to move";
        }
    }

}
echo $response;



?>