<?php

 include("../connections/conn.php");

if (isset($_POST['name'])) {
    $name = $_POST['name'];

   
    $sql_check = $conn->prepare('SELECT * FROM Model WHERE Name =?');
    $sql_check->bind_param("s", $name);
    $sql_check->execute();
    $sql_check->store_result();
    $num_rows = $sql_check->num_rows;
    $sql_check->close();


    if ($num_rows > 0) {
        echo '<span style="color: red;">The username <b>' . $name . '</b> is already in use.</span>';
    } else {
        echo "OK";
    }
}


 $response = "";
if (isset($_POST['check_id'])) {
    $id= $_POST['check_id'];
   

    $check_access = "SELECT Is_Public FROM Model WHERE ID = '$id'";
    $result=$conn->query($check_access);
    while($row=$result->fetch_assoc()){
        $access =$row['Is_Public'];
    }
    if(!$result){
        echo $conn->error;
    }else{
        if($access==0){
            $response .= "0";
        }else{
            $response .= "1";
        }
    }
}
echo $response;

?>