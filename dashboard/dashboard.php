<?php
session_start();

if (!isset($_SESSION['authenticated'])) {
    header('location:../login.php');
}


if (isset($_SESSION['authenticated'])) {
    $login_id = $_SESSION['authenticated'];
}
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Dashboard</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="uploadstyles/dashboard.css" rel="stylesheet">

    <style>
        body {
            font-family: 'Courier New', Courier, monospace;
        }

        #toprow {

            margin-top: 2%;
            padding-bottom: 1%;

        }

        #midrow {
            padding-bottom: 1%;
        }

        #dash {
            font-size: 30px;
            color: rgb(12, 45, 192);
            font-family: 'Courier New', Courier, monospace;
            margin-left: 17%;
            margin-right: 0%;

        }

        #navbtn {
            background-color: rgba(10, 26, 99, 0.8);

        }
    </style>
</head>

<body>
    <header>
        <div class="collapse bg-dark" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-7 py-4">
                        <h4 class="text-white">Get started</h4>
                        <p class="text-muted">
                            Go to the Image analysis pipeline or ML Model page to upload and manage your files
                        </p>
                    </div>
                    <div class="col-sm-4 offset-md-1 py-4">

                        <ul class="list-unstyled">
                            <li><a href="../logout.php" class="text-white">Log out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar  box-shadow" style=>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" id="navbtn" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <p id="dash">
                <strong>Dashboard</strong>
            </p>
        </div>
    </header>
    <main role="main">
        <div id="layoutSidenav_content">
            <main>
                <div class="row" id="toprow">

                    <div class="col-xl-2 col-md-6" style="margin-left: 14%;">
                        <a href="pipelineuploader.php" style="text-decoration:none;">
                            <div class="card text-white mb-4" style="height: 100%; background-color: rgba(14, 97, 145,0.8);">
                                <div class="card-body" style="text-align: center;">
                                    <p style="color:back;margin: 0px 0px 0px 0px;"> IAM Pipeline Manager</p>
                                </div>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row" id="midrow">
                    <div class="col-xl-2 col-md-6" style="margin-left: 14%;">
                        <a href="modeluploader.php" style="text-decoration:none;">
                            <div class="card text-white mb-4" style="height: 100%;background-color: rgba(88, 83, 83, 0.692);">
                                <div class="card-body" style="text-align: center;">
                                    <p style="color:white;margin: 0px 0px 0px 0px;"> ICM Model Manager</p>
                                </div>
                                <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                            </div>
                        </a>
                    </div>
                </div>   
            </main>
            <footer class="text-muted">
                <div class="container">
                </div>
            </footer>
        </div>
    </main>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
</body>

</html>