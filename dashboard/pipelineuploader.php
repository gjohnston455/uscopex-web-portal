<?php
session_start();

if (!isset($_SESSION['authenticated'])) {
    header('location:../login.php');
}


if (isset($_SESSION['authenticated'])) {
    $login_id = $_SESSION['authenticated'];
}
?>


<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>IAP uploader</title>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous">
        < script src = "https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"/ >
    </script>
    <!-- Custom styles for this template -->
    <link href="uploadstyles/styles.css" rel="stylesheet">
</head>

<script src="js/dashboard.js" type="text/javascript"></script>


<body>

    <header>
        <div class="collapse bg-dark" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-7 py-4">
                        <h4 class="text-white">Get started</h4>
                    </div>
                    <div class="col-sm-4 offset-md-1 py-4">

                        <ul class="list-unstyled">
                            <li><a href="../logout.php" class="text-white">Log out</a></li>
                            <li><a href="dashboard.php" class="text-white">Back to base</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar box-shadow" style="  background-color: rgba(14, 97, 145, 0.877); ">
            <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">

                    <strong>IAM Manager</strong>
                </a>
                <button class="navbar-toggler" style="background-color: rgb(10, 75, 75);" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">

                </button>
            </div>
        </div>
    </header>

    <main role="main">

        <section class="jumbotron text-center" style="background-color: rgb(60, 49, 49);">
            <div class="container">
                <h1 class="jumbotron-heading" style="color:  rgb(61, 118, 141);">Upload a new pipeline</h1>
                    <a class="btn btn-light my-2" style="background-color: rgba(11, 38, 73, 0.877);color:white;border: 0px;" data-toggle="modal" href="#pipeline" data-target="#">Upload</a>
                </p>
            </div>

            <!-- Upload pipeline modal -->
            <div class="modal fade" id="pipeline" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body modal-body2">

                            <div class='row'>
                                <div class='col'>
                                    <form action='pipelineuploader.php' method="POST" enctype='multipart/form-data'>

                                        <div class="form-group">

                                            <input type="file" class="form-control-file" name="myfileupload" id="exampleFormControlFile1">
                                        </div>

                                </div>
                            </div>
                            <div class='row' style="padding-bottom: 5%;">
                                <div class='col' id="pop">
                                    <input type="text" class="form-control" id="name" name="pipeline_name" placeholder="Name" aria-label="Search">
                                </div>

                            </div>

                            <div class='row'>
                                <div class='col'>
                                    <div class="form-group">

                                        <textarea class="form-control" id="description" name="description" rows="3" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class='row' style="padding-left: 5%;">
                                <div class="form-group form-check">
                                    <input type="checkbox" name="public" class="form-check-input" id="exampleCheck1" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Public</label>
                                </div>
                            </div>
                            <div class='row' style="padding-left: 5%;">
                                <div class="form-group form-check">
                                    <input type="checkbox" name="batchable" class="form-check-input" id="exampleCheck1" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Batch ready</label>
                                </div>
                            </div>

                            <div class='row' style="padding-left: 5%;">
                                <div class="form-group form-check">
                                    <input type="checkbox" name="roi" class="form-check-input" id="exampleCheck1" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">ROI Required</label>
                                </div>
                            </div>
                            <div class='row' style="padding-left: 5%;">
                                <div class="form-group form-check">
                                    <input type="checkbox" name="scale" class="form-check-input" id="exampleCheck1" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Scale Required</label>
                                </div>
                            </div>
                            <div class='row' style="padding-left: 5%;">
                                <div class="form-group form-check">
                                    <input type="checkbox" name="results" class="form-check-input" id="exampleCheck1" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Produces result file</label>
                                </div>
                            </div>

                            <div class="row" style="padding-left: 5%;">

                                <div class="form-group form-check">
                                    <input type="checkbox" name="image" class="form-check-input" id="exampleCheck1" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Produces image file</label>
                                </div>
                            </div>

                            <div class='row' style="padding-bottom: 5%;">
                                <div class='col'>
                                    <input type="email" class="form-control" id="user_access" name="user_access" placeholder="Grant user access (email)" aria-label="Search">
                                </div>
                            </div>

                            <div class='row'>
                                <div class='col'>
                                    <button type="submit" name="fileup" style="float: left; background-color:  rgba(63, 40, 32, 0.863); color: whitesmoke;" id="upload" class="btn ">Upload</button>
                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Change pipeline modal -->
            <div class="modal fade edit" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body modal-body4">

                            <div class='row' style="padding-bottom: 5%;">
                                <div class='col' id="pop">
                                    <input type="text" class="form-control" id="change_name" name="change_name" placeholder="Name" aria-label="Search">
                                </div>

                            </div>

                            <div class='row'>
                                <div class='col'>
                                    <div class="form-group">

                                        <textarea class="form-control" id="change_desc" name="change_description" rows="3" placeholder="Description"></textarea>
                                    </div>
                                </div>
                            </div>

                            </form>
                            <div class='row'>
                                <div class='col'>
                                    <button name="butt" style="float: left; background-color:  rgba(63, 40, 32, 0.863); color: whitesmoke;" id="changer" class="btn change">Change</button>

                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Change version modal -->
            <div class="modal fade version" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Version Update</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body modal-body3">

                            <div class='row'>
                                <div class='col'>
                                    <form action='pipelineuploader.php' method="POST" enctype='multipart/form-data'>
                                        <div class="form-group">
                                            <input type="file" class="form-control-file version_updater" name="versioner" id="versioner">
                                        </div>
                                </div>
                            </div>
                            <div class='row' style="padding-left: 5%;">

                                <div class="form-group form-check">
                                    <input type="checkbox" name="roi_version" class="form-check-input" id="roi_version" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">ROI Required</label>
                                </div>

                            </div>
                            <div class="row" style="padding-left: 5%;">
                                <div class="form-group form-check">
                                    <input type="checkbox" name="scale_version" class="form-check-input" id="scale_version" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Scale Required</label>
                                </div>
                            </div>
                            <div class='row' style="padding-left: 5%;">
                                <div class="form-group form-check">
                                    <input type="checkbox" name="results_version" class="form-check-input" id="results_version" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Produces result file</label>
                                </div>
                            </div>

                            <div class="row" style="padding-left: 5%;">

                                <div class="form-group form-check">
                                    <input type="checkbox" name="image_version" class="form-check-input" id="image_version" value="Yes">
                                    <label class="form-check-label" for="exampleCheck1">Produces image file</label>
                                </div>
                            </div>
                            <div class='row'>
                                <div class='col'>
                                    <input type="text" class="form-control" id="version_number" name="version_number" placeholder="Version (0.0.0)" aria-label="Search">
                                </div>
                            </div>
                            <div class='row' style="padding-top: 5%;">
                                <div class='col'>
                                    <input type="text" class="form-control" id="version_description" name="version_description" placeholder="Description" aria-label="Search">
                                </div>
                            </div>

                            <div class='row' style="padding-top: 5%;">
                                <div class='col'>
                                    <button type="button" name="update_version" style="float: left; background-color:  rgba(63, 40, 32, 0.863); color: whitesmoke;" id="updater_btn" class="btn ">Update</button>

                                </div>
                            </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" name="" class="btn btn-secondary close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Change Access modal -->
            <div class="modal fade access" id="access" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Access</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body modal-body3">


                            <form>
                                <div class="custom-control custom-switch" id="access_change">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-primary active access_manager_change" data-id="1">
                                            <input type="radio" id="option1"> Public
                                        </label>
                                        <label class="btn btn-primary access_manager_change" data-id="0">
                                            <input type="radio" name="options" id="option2"> Private
                                        </label>

                                    </div>
                                </div>

                                <div class='row' style="padding-bottom: 5%; padding-top: 10%;">
                                    <div class='col-8'>
                                        <input type="email" class="form-control" id="add_user_access" name="add_user_access" placeholder="Grant user access (email)" aria-label="Search">
                                    </div>

                                    <div class='col-4'>
                                        <button type="button" id="user_access_btn" class="btn btn-info user_access_btn">Go</button>
                                    </div>
                                </div>
                                <div class='row' style="padding-bottom: 5%;">
                                    <div class='col' id="access_response">
                                    </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" name="" class="btn btn-secondary close" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
        include("../connections/conn.php");


        if (isset($_POST['fileup'])) {
            $filename = $_FILES['myfileupload']['name'];
            $filedata = $_FILES['myfileupload']['tmp_name'];
            $ftype = $_FILES['myfileupload']['type'];
            $name = $_POST['pipeline_name'];
            $desc = $_POST['description'];

            if (isset($_POST['public']) && $_POST['public'] == 'Yes') {
                $public = 1;
            } else {
                $public = 0;
            }
            if (isset($_POST['roi']) && $_POST['roi'] == 'Yes') {
                $roi = 1;
            } else {
                $roi = 0;
            }
            if (isset($_POST['scale']) && $_POST['scale'] == 'Yes') {
                $scale = 1;
            } else {
                $scale = 0;
            }
            if (isset($_POST['batchable']) && $_POST['batchable'] == 'Yes') {
                $batchable = 1;
            } else {
                $batchable = 0;
            }
            if (isset($_POST['results']) && $_POST['results'] == 'Yes') {
                $results = 1;
            } else {
                $results = 0;
            }
            if (isset($_POST['image']) && $_POST['image'] == 'Yes') {
                $image = 1;
            } else {
                $image = 0;
            }

            $validate_unique_names = $conn->prepare('SELECT * FROM Pipeline WHERE Name = ?');


            $validate_unique_names->bind_param("s", $name);
            $validate_unique_names->execute();
            $validate_unique_names->store_result();
            $name_validation = $validate_unique_names->num_rows;
            $validate_unique_names->close();
            $file_path = $name . "_1.0.0.bsh";
            echo "$file_path";
            if ($name_validation < 1) {
                $moved = move_uploaded_file($filedata, '../../iam_pipeline/' . $file_path);

                if ($moved) {
                    echo "moved";
                    $conn->begin_transaction();

                    $insert_pipeline = $conn->prepare('INSERT INTO Pipeline 
                    (Name, Description, Versions, Date_Added, Is_Public, Author, Batchable) 
                    VALUES
                    (?,?,1.0,NOW(),?,?,?)');

                    $insert_pipeline->bind_param("ssiii", $name, $desc, $public, $login_id, $batchable);

                    if ($insert_pipeline->execute()) {
                        echo "IN DB";
                        $pipeline_id = $conn->insert_id;
                        $insert_pipeline->close();
                        $uploader_access = $conn->prepare('INSERT INTO Pipeline_Access (Pipeline_ID, User_ID) VALUES (?,?)');
                        $uploader_access->bind_param("ii", $pipeline_id, $login_id);

                        if ($uploader_access->execute()) {
                            $version = "1.0.0";
                            echo "Access provided to uploader";
                            $update_version_table = $conn->prepare('INSERT INTO Pipeline_Version (Pipeline,Version,Description,Date) Values (?,?,?,NOW())');
                            $update_version_table->bind_param("iss", $pipeline_id, $version, $desc);

                            if ($update_version_table->execute()) {
                               
                                $version_id = $conn->insert_id;

                                $insert_pipeline_outputs = $conn->prepare('INSERT INTO Pipeline_Output (Pipeline,Image, Result_Set) VALUES (?,?,?)');
                                $insert_pipeline_outputs->bind_param("iii", $version_id, $image, $results);

                                if ($insert_pipeline_outputs->execute()) {
                                 
                                    $insert_pipeline_outputs->close();
                                    $insert_requirements = $conn->prepare('INSERT INTO Pipeline_Requirements (Pipeline_ID,Requires_Roi,Requires_Scale) VALUES (?,?,?)');

                                    $insert_requirements->bind_param("iii", $version_id, $roi, $scale);
                                    if ($insert_requirements->execute()) {
                                        $conn->commit();
                                        
                                    } else {
                                        $conn->rollback();
                                        echo "Requirements not set";
                                    }
                                    if (isset($_POST['user_access'])) {
                                        $user_email = $_POST['user_access'];

                                        $user_access = $conn->prepare('INSERT INTO Pipeline_Access (Pipeline_ID, User_ID) VALUES (?,(SELECT ID FROM Login WHERE Email = ?))');
                                        $user_access->bind_param("is", $pipeline_id, $user_email);

                                        if ($user_access->execute()) {
                                            echo "Pipeline uploaded successfully";
                                        } else {
                                            echo "User email doesn't exist";
                                        }
                                    }
                                } else {
                                    echo "Outputs not updating";
                                    $conn->rollback();
                                }
                            } else {
                                echo "Version table not updated";
                                echo $conn->error;
                                $conn->rollback();
                            }
                        } else {
                            echo "Failed to provide access to uploader";
                            $conn->rollback();
                        }
                    } else {

                        echo $conn->errror . " Failed to upload pipeline";
                        $conn->rollback();
                    }
                } else {
                    echo "File not uploading";
                }
            } else {
                echo "Please change the name of this pipeline";
            }
        }
        ?>

        <section class=" text-center">
            <div class="container">
                <h1 class="jumbotron-heading">Active pipelines</h1>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">

                <div class="row">

                    <?php

                    $pipeline_details = $conn->prepare('SELECT * FROM Pipeline WHERE Author = ?');

                    $pipeline_details->bind_param("i", $login_id);
                    $pipeline_details->execute();
                    $pipeline_details->bind_result($id, $pipeline_name, $description, $version, $date, $privicy, $author, $batchable);

                    while ($pipeline_details->fetch()) {

                        echo " <div class='col-md-4'>
                        <div class='card mb-4 box-shadow' style='background-color:  rgba(200, 236, 216, 0.788);'>

                            <div class='card-body'>
                                <p class='card-text'><span style='color:green;'>Name</span>: $pipeline_name</p>
                                <p class='card-text'><span style='color:green;'>Versions</span>: $version</p> 
                                <p class='card-text'><span style='color:green;'>Last version update</span>: $date</p>
                                <div class='d-flex justify-content-between align-items-center' style='margin-top: 0px;'>
                                <div class='btn-group' style='margin-top: 0px;'>
                                <button type='button' data-toggle='modal' data-target='.access'data-toggle='modal' href='#pipeline' data-target='#access' data-id='$id'class='btn btn-sm btn-outline-secondary access_manager'>Access</button>       
                                <button type='button' data-toggle='modal' data-target='.edit'data-toggle='modal' href='#pipeline' data-target='#edit' data-id='$id'class='btn btn-sm btn-outline-secondary editer'>Edit</button>
                                <button type='button' data-toggle='modal' data-target='.version'data-toggle='modal' href='#version' data-target='#version_update' data-id='$id'class='btn btn-sm btn-outline-secondary version_updater_btn'>Version update</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                       ";
                    } ?>
                </div>
            </div>
        </div>

    </main>


    <footer class="text-muted">
        <div class="container">
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous">
    </script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/popper.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/dist/js/bootstrap.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.0/assets/js/vendor/holder.min.js"></script>
</body>

</html>